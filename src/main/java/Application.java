import activity.Activity;
import activity.ActivityException;
import activity.MainActivity;
import activity.MainActivityImpl;
import gui.AbstractGuiFactory;
import gui.MenuItem;
import gui.Window;
import presenter.MainWindowPresenter;

public class Application {

    private AbstractGuiFactory guiFactory;
    private MainActivity activity;

    public Application(AbstractGuiFactory guiFactory) {
        this.guiFactory = guiFactory;
        activity = new MainActivityImpl();
    }

    public void start()
    {
        activity.createViews(guiFactory);
        try {
            activity.start();
        } catch (ActivityException e) {
            System.out.println("Failed to start application. Reason: "
                + e.getMessage());
        }
    }

//    private void buildViews() {
//        MainWindowPresenter presenter = new MainWindowPresenter();
//        mainWindow = guiFactory.getWindow("Simple Text Editor");
//        mainWindow.setPresenter(presenter);
//        mainWindow.setSize(800, 600);
//        MenuItem item = guiFactory.getMenuItem("AAA");
//        item.setOnClickListener(presenter);
        // create windows and subviews
//    }
}
