package presenter;

import gui.Window;

public interface WindowPresenter {
    void setWindow(Window window);
    void updateWindowTitle(String title);
    void onMenuItemSelected();
}
