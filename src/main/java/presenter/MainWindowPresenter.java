package presenter;

import gui.Menu;
import gui.OnClickListener;
import gui.View;
import gui.Window;

import java.util.List;

public class MainWindowPresenter implements WindowPresenter, OnClickListener {

    Window window;

    @Override
    public void setWindow(Window window) {
        this.window = window;
        List<Menu> menus = window.getMenubar().getMenus();
        for (Menu menu : menus) {
            //setMenuItemsListeners(menu.getMenuItems());
        }
    }

    @Override
    public void updateWindowTitle(String title) {
        window.setTitle(title);
    }

    @Override
    public void onMenuItemSelected() {

    }

    @Override
    public void onClicked(View clicked) {

    }
}
