package gui;

import java.util.List;

public interface Menu {
    void addMenuItem(MenuItem item);
    List<MenuItem> getMenuItems();
}
