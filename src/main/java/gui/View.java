package gui;

public interface View {
    void setSize(int width, int height);
    void setOnClickListener(OnClickListener listener);
    void setEnabled(Boolean enabled);
}
