package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class SwingMenuBarImpl implements Menubar, SwingMenuBar, SwingComponent,
        ActionListener {

    JMenuBar menuBar;
    private OnClickListener clickListener = null;
    private List<Menu> menuList = new ArrayList<>();

    SwingMenuBarImpl(JMenuBar panel) {
        menuBar = panel;
    }

    @Override
    public void addMenu(Menu menu) {
        if (!menuList.contains(menu)) {
            menuList.add(menu);
            menuBar.add(((SwingComponent)menu).getComponent());
        }
    }

    @Override
    public List<Menu> getMenus() {
        return menuList;
    }

    @Override
    public void setSize(int width, int height) {
        menuBar.setSize(width, height);
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        clickListener = listener;
    }

    @Override
    public void setEnabled(Boolean enabled) {
        menuBar.setEnabled(enabled);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this && clickListener != null) {
            clickListener.onClicked(this);
        }
    }

    @Override
    public Component getComponent() {
        return menuBar;
    }

    @Override
    public JMenuBar getMenuBar() {
        return menuBar;
    }
}
