package gui;

public interface TextArea extends TextView {
    void setLineWrap(boolean doWrap);
}
