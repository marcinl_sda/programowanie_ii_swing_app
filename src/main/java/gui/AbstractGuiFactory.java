package gui;

public interface AbstractGuiFactory {
    Button getButton(String label);
    Window getWindow(String title);
    Menubar getMenubar();
    TextArea getTextArea();
    Menu getMenu(String name);
    MenuItem getMenuItem(String name);
}
