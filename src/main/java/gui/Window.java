package gui;

import presenter.WindowPresenter;

public interface Window extends View {
    void setPresenter(WindowPresenter presenter);
    void setTitle(String title);
    void setPosition(int x, int y);
    void show();
    void hide();
    void addView(View view);
    void removeView(View view);
    void setMenubar(Menubar toolbar);
    Menubar getMenubar();
}
