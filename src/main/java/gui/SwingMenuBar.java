package gui;

import javax.swing.*;

interface SwingMenuBar {
    JMenuBar getMenuBar();
}
