package gui;

import java.awt.*;

interface SwingComponent {
    Component getComponent();
}
