package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwingButton  implements Button, ActionListener, SwingComponent {

    private JButton button;
    private OnClickListener listener = null;

    SwingButton(JButton button) {
        this.button = button;
    }

    @Override
    public void click() {
        button.doClick();
    }

    @Override
    public void setText(String text) {
        button.setText(text);
    }

    @Override
    public String getText() {
        return button.getText();
    }

    @Override
    public void setSize(int width, int height) {
        button.setSize(width, height);
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
        if (listener == null) {
            button.removeActionListener(this);
        } else {
            button.addActionListener(this);
        }
    }

    @Override
    public void setEnabled(Boolean enabled) {
        button.setEnabled(enabled);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button) {
            if (listener != null) {
                listener.onClicked(this);
            }
        }
    }

    @Override
    public Component getComponent() {
        return button;
    }
}
