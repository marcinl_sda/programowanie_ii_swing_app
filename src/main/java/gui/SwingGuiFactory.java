package gui;

import javax.swing.*;

public class SwingGuiFactory implements AbstractGuiFactory {

    @Override
    public Button getButton(String label) {
        JButton jButton = new JButton(label);
        return new SwingButton(jButton);
    }

    @Override
    public Window getWindow(String title) {
        JFrame jFrame = new JFrame(title);
        return new SwingWindow(jFrame);
    }

    @Override
    public Menubar getMenubar() {
        JMenuBar menuBar = new JMenuBar();
        return new SwingMenuBarImpl(menuBar);
    }

    @Override
    public TextArea getTextArea() {
        JTextArea jTextArea = new JTextArea();
        return new SwingTextArea(jTextArea);
    }

    @Override
    public Menu getMenu(String name) {
        JMenu menu = new JMenu(name);
        return new SwingMenu(menu);
    }

    @Override
    public MenuItem getMenuItem(String name) {
        JMenuItem item = new JMenuItem(name);
        return new SwingMenuItem(item);
    }
}
