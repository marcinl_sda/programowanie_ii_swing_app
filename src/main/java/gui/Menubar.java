package gui;

import java.util.List;

public interface Menubar extends View {
    void addMenu(Menu menu);
    List<Menu> getMenus();
}
