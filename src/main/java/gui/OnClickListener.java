package gui;

public interface OnClickListener {
    void onClicked(View clicked);
}
