package gui;

import presenter.WindowPresenter;

import javax.swing.*;

public class WindowImpl implements Window {

    private JFrame window;
    private Menubar menubar;
    private WindowPresenter presenter;

    WindowImpl(JFrame frame) {
        window = frame;
    }

    @Override
    public void setPresenter(WindowPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setTitle(String title) {
        window.setTitle(title);
    }

    @Override
    public void setPosition(int x, int y) {
        window.setLocation(x, y);
    }

    @Override
    public void show() {
        window.setVisible(true);
    }

    @Override
    public void hide() {
        window.setVisible(false);
    }

    @Override
    public void addView(View view) {
        if (view instanceof SwingComponent) {
            window.add(((SwingComponent)view).getComponent());
        }
    }

    @Override
    public void removeView(View view) {
        if (view instanceof SwingComponent) {
            window.remove(((SwingComponent)view).getComponent());
        }
    }

    @Override
    public void setMenubar(Menubar menubar) {
        if (this.menubar == null) {
            this.menubar = menubar;
            window.setJMenuBar(((SwingMenuBar)menubar).getMenuBar());
        }
    }

    @Override
    public Menubar getMenubar() {
        return menubar;
    }

    @Override
    public void setSize(int width, int height) {
        window.setSize(width, height);
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
    }

    @Override
    public void setEnabled(Boolean enabled) {
        window.setEnabled(enabled);
    }
}
