package gui;

import javax.swing.*;
import java.awt.*;

public class SwingTextArea implements TextArea, SwingComponent {

    private JTextArea internalText;

    SwingTextArea(JTextArea textArea) {
        internalText = textArea;
    }

    @Override
    public void setText(String text) {
        internalText.setText(text);
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    public void setSize(int width, int height) {

    }

    @Override
    public void setOnClickListener(OnClickListener listener) {

    }

    @Override
    public void setEnabled(Boolean enabled) {
        internalText.setEnabled(enabled);
    }

    @Override
    public void setLineWrap(boolean doWrap) {
        internalText.setLineWrap(doWrap);
    }

    @Override
    public Component getComponent() {
        return internalText;
    }
}
