package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwingMenuItem implements MenuItem, SwingComponent, ActionListener {

    private JMenuItem menuItem;
    private OnClickListener clickListener = null;

    SwingMenuItem(JMenuItem menuItem) {
        this.menuItem = menuItem;
    }

    @Override
    public Component getComponent() {
        return menuItem;
    }

    @Override
    public void setText(String text) {
        menuItem.setText(text);
    }

    @Override
    public String getText() {
        return menuItem.getText();
    }

    @Override
    public void setSize(int width, int height) {
        menuItem.setSize(width, height);
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        clickListener = listener;
        menuItem.addActionListener(this);
    }

    @Override
    public void setEnabled(Boolean enabled) {
        menuItem.setEnabled(enabled);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == menuItem && clickListener != null) {
            clickListener.onClicked(this);
        }
    }
}
