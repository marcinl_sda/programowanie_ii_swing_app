package gui;

public interface MenuItem extends TextView {
    /**
     * Menu item type
     */
    public enum MenuItemType {
        New,
        Open,
        Save,
        Exit
    }
}
