package gui;

import javax.swing.JMenu;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

public class SwingMenu implements Menu, SwingComponent {

    JMenu menu;
    private List<MenuItem> menuItems = new ArrayList<>();

    SwingMenu(JMenu menu) {
        this.menu = menu;
    }

    @Override
    public void addMenuItem(MenuItem item) {
        if (!menuItems.contains(item)) {
            menu.add(((SwingComponent) item).getComponent());
        }
    }

    @Override
    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    @Override
    public Component getComponent() {
        return menu;
    }
}
