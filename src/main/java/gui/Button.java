package gui;

public interface Button extends TextView {
    void click();
}
