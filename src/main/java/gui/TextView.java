package gui;

public interface TextView extends View {
    void setText(String text);
    String getText();
}
