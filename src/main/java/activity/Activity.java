package activity;

import gui.AbstractGuiFactory;

public interface Activity {
    void start() throws ActivityException;
    void createViews(AbstractGuiFactory factory);
}
