package activity;

import gui.AbstractGuiFactory;
import gui.Menu;
import gui.MenuItem;
import gui.Menubar;
import gui.TextArea;
import gui.Window;

public class MainActivityImpl implements MainActivity {

    Window mainWindow;
    TextArea textArea;

    @Override
    public void start() throws ActivityException {
        if (mainWindow != null) {
            mainWindow.show();
        } else {
            throw new ActivityException("No valid window!");
        }
    }

    @Override
    public void createViews(AbstractGuiFactory factory) {
        mainWindow = factory.getWindow("Editor");
        mainWindow.setSize(800, 600);
        textArea = factory.getTextArea();
        mainWindow.addView(textArea);

        Menubar menubar = factory.getMenubar();
        Menu fileMenu = factory.getMenu("File");
        //MenuItem newFile = factory.getMenuItem()
    }
}
