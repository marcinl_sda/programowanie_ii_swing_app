import gui.*;

public class Main {
    public static void main(String[] args) {
        AbstractGuiFactory factory = new SwingGuiFactory();
        Application app = new Application(factory);
        app.start();
    }
}